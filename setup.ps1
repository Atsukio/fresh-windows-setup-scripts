# Disable Show More Context menu
reg delete "HKEY_CURRENT_USER\Software\Classes\CLSID\{86ca1aa0-34aa-4e8b-a509-50c905bae2a2}" /f​

# Disable hortcut text when creating shortcuts
reg add "HKEY_CURRENT_USER\SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer" /t REG_BINARY /v link /d 00000000 /f

# Setup Chocolatey
Set-ExecutionPolicy Bypass -Scope Process -Force; [System.Net.ServicePointManager]::SecurityProtocol = [System.Net.ServicePointManager]::SecurityProtocol -bor 3072; iex ((New-Object System.Net.WebClient).DownloadString('https://community.chocolatey.org/install.ps1'))

# Install Chocolatey GUI
choco install chocolateygui -y

# Setup GIT
git config --global user.name "Atsukio Potato"
git config --global user.email "saythen@gmail.com"
